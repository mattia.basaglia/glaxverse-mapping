

.PHONY: all upload/master.svg upload/master.png

all: output/master.svg
all: output/master.png

output:
	mkdir output

output/master.svg: master.svg
output/master.svg: output
output/master.svg: link.py
	./link.py

output/master-large.png: master.kra
	krita  --export --export-filename output/master-large.png master.kra

output/master.png: output/master-large.png
# 	gimp -i -b '(let* ((sourcefilename "output/master-large.png")(destfilename "output/master.png")(targetwidth 1300)(image (car (file-png-load 1 sourcefilename sourcefilename)))(drawable (car (gimp-image-active-drawable image)))(oldwidth (car (gimp-image-width image)))(oldheight (car (gimp-image-height image)))(newheight (* targetwidth (/ oldheight oldwidth))))(gimp-image-scale image targetwidth newheight)(file-png-save 1 image drawable destfilename destfilename  1 0 0 0 0 0 0 ))' -b "(gimp-quit 0)"
# 	convert output/master-large.png -geometry 1300x output/master.png
	./split.py output/master-large.png -w 1300 -o output/master.png
	optipng output/master.png

output/master1.png: output/master.png
	gimp -i -b "(gimp-image-resize (file-png-load RUN-NONINTERACTIVE 'output/master.png' '$(pwd)/output/master.png') 1300 1737)" -b '(gimp-quit 0)'


upload/master.svg: output/master.svg
	scp $< scp://mattbas.org//var/www/mediawiki/__mattbas_settings/glaxverse.dragon.best/media/maps

upload/master.png: output/master.png
	scp $< scp://mattbas.org//var/www/mediawiki/__mattbas_settings/glaxverse.dragon.best/media/maps
