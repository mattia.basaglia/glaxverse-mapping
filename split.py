#!/usr/bin/env python3
import os
import json
import argparse
import PIL.Image
import PIL.ImageColor


parser = argparse.ArgumentParser()
parser.add_argument("input")
parser.add_argument("--output", "-o")
parser.add_argument("--width", "-w", required=True, type=int)
parser.add_argument("--chunk-width", "-c", default=1024, type=int)
parser.add_argument("--output-path", default="output")
parser.add_argument("--background", default="#d9c5a2")

ns = parser.parse_args()

PIL.Image.MAX_IMAGE_PIXELS = None
image = PIL.Image.open(ns.input)

outfile = ns.output or os.path.join(ns.output_path, os.path.basename(ns.input))
basename, ext = os.path.splitext(os.path.basename(outfile))
os.makedirs(os.path.join(ns.output_path, "chunks"), exist_ok=True)
bgcolor = PIL.ImageColor.getrgb(ns.background)
for y in range(0, image.height, ns.chunk_width):
    for x in range(0, image.width, ns.chunk_width):
        #chunk = image.crop(
            #(x, y, x+ns.chunk_width, y+ns.chunk_width)
        #)
        chunk = PIL.Image.new('RGB', (ns.chunk_width, ns.chunk_width), bgcolor)
        chunk.paste(image, (-x, -y))
        chunk.save(
            os.path.join(ns.output_path, "chunks", "%s+%03d,%03d%s" % (basename, x, y, ext))
        )

with open(os.path.join(ns.output_path, "chunks", basename + ".json"), "w") as info:
    json.dump(
        {"width": image.width, "height": image.height, "chunk": ns.chunk_width},
        info
    )

width = ns.width
height = int(round(ns.width*image.height/image.width))
image.resize((width, height), PIL.Image.BILINEAR).save(outfile)

