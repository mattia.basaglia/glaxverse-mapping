#!/usr/bin/env python3
import os
from lxml import etree as et
# from xml.dom.minidom import parse


class GoddamnWrapper:
    __slots__ = ["_node", "nsmap"]

    def __init__(self, node, nsmap):
        self._node = node
        self.nsmap = nsmap

    def __getattr__(self, key):
        if hasattr(self.__class__, key):
            prop = getattr(self.__class__, key)
            if isinstance(prop, property):
                return prop.__get__(self)
        return getattr(self._node, key)

    def find(self, query):
        return GoddamDOMElement(self._node.find(query, self.nsmap))

    def findall(self, query):
        return map(GoddamDOMElement, self._node.findall(query, self.nsmap))

    @staticmethod
    def _extract(obj):
        if isinstance(obj, GoddamnWrapper):
            return obj._node
        return obj

    @staticmethod
    def _wrap(obj):
        if isinstance(obj, GoddamnWrapper):
            return obj
        elif isinstance(obj, et.Element):
            return GoddamDOMElement(obj)
        elif isinstance(obj, et.ElementTree):
            return GoddamDOMDocument(obj)

    def _prefix_expand(self, name):
        if ":" in name:
            ns, name = name.split(":")
            return "{%s}%s" % (self.nsmap[ns], name)
        return name

    def _prefix_collapse(self, name):
        if "}" in name:
            ns, name = name.split("}")
            ns = ns.lstrip("{")
            for k, v in self.nsmap.items():
                if v == ns:
                    break
            return "%s:%s" % (k, name)
        return name


class GoddamDOMDocument(GoddamnWrapper):
    def __init__(self, node):
        super().__init__(node, node.getroot().nsmap)

    def createElement(self, tag):
        return GoddamDOMElement(et.Element(tag, nsmap=self.nsmap))

    def createTextNode(self, text):
        return GoddamDOMTextNode(text)

    @property
    def documentElement(self):
        return GoddamDOMElement(self._node.getroot())

    def writexml(self, file):
        file.write(et.tostring(self._node).decode("utf8"))


class GoddamDOMTextNode:
    def __init__(self, str):
        self._str = str


class GoddamDOMElement(GoddamnWrapper):
    __slots__ = ["_node"]

    def __init__(self, node):
        super().__init__(node, node.nsmap)

    @property
    def parentNode(self):
        return GoddamDOMElement(self._node.getparent())

    @property
    def textContent(self):
        return "".join(self._node.itertext())

    def setAttribute(self, name, value):
        return self._node.set(self._prefix_expand(name), value)

    def getAttribute(self, name,):
        return self._node.get(self._prefix_expand(name))

    def appendChild(self, node):
        if isinstance(node, GoddamDOMTextNode):
            if len(self._node):
                self._node[-1].tail += node._str
            elif self._node.text:
                self._node.text += node._str
            else:
                self._node.text = node._str
            return
        self._node.append(self._extract(node))
        return self._wrap(node)

    def insertBefore(self, newNode, referenceNode):
        n = self._extract(newNode)
        ref = self._extract(referenceNode)
        index = self._node.index(ref)
        return self._node.insert(index, n)

    def removeChild(self, child):
        self._node.remove(self._extract(child))

    def replaceChild(self, newChild, oldChild):
        self.insertBefore(newChild, oldChild)
        self.removeChild(oldChild)

    def __repr__(self):
        return "<Element %s>" % self._prefix_collapse(self._node.tag)


def parse(filename):
    return GoddamDOMDocument(et.parse(filename))


def link(element, target):
    a = document.createElement("a")
    slug = target.replace(" ", "_")

    a.setAttribute("xlink:href", "https://glaxverse.dragon.best/" + slug)
    a.setAttribute("xlink:title", target)
    a.setAttribute("target", "_parent")
    a.setAttribute("id", slug)
    element.parentNode.replaceChild(a, element)
    a.appendChild(element)
    return a


document = parse("master.svg")


for text in document.findall("//svg:text"):
    link(text, text.textContent)


for title in document.findall("//svg:title"):
    title.parentNode.setAttribute("id", title.textContent.replace(" ", "_"))


document.documentElement.appendChild(document.createElement("style")).appendChild(document.createTextNode(
    "\n".join(
        "@namespace %s \"%s\";" % it
        for it in document.nsmap.items()
    )+"""
@font-face {
    font-family: "Linux Biolinum O";
    src: url("/skins/erudite/assets/fonts/LinBiolinum_R_subset.woff");
}
a:target text, .highlight text {
    fill: #a03529 !important;
}
svg|g[inkscape|label="areas"] > g > * {
    opacity: 0 !important;
    pointer-events: none;
}
svg|g[inkscape|label="areas"] > g > *:target,
svg|g[inkscape|label="areas"] > g > *.highlight {
    opacity: 1 !important;
}
svg|g[inkscape|label="areas"],
svg|g[inkscape|label="areas"] > g,
svg|g[inkscape|label="map"],
svg|g[inkscape|label="labels"]
{
    display: inline !important;
}
"""
))


with open(os.path.join(os.path.dirname(__file__), "output", "master.svg"), "w") as out:
    document.writexml(out)
